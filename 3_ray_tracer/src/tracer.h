#pragma once

#include "glm/glm.hpp"
#include "types.h"
#include "scene.h"
#include "image.h"


class Tracer
{
public:
    SRay createRay(glm::uvec2 pixelPos);  // Create ray for specified pixel
    glm::vec3 TraceRay(SRay ray); // Trace ray, compute its color
    Image renderImage(int xRes, int yRes);

    Scene* getScene();
    void setScene(Scene* scene);

protected:
    Camera _camera;
    Scene* _scene;
};
