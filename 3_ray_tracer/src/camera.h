#pragma once

#include "glm/glm.hpp"

//sudo aptitude install libilmbase-dev openexr libopenexr-dev libx11-dev
//libpng-dev
struct Camera
{
    glm::vec3 _pos;          // Camera position and orientation
    glm::vec3 _forward;      // Orthonormal basis
    glm::vec3 _right;
    glm::vec3 _up;

    glm::vec2 _viewAngle;    // View angles, rad
    glm::uvec2 _resolution;  // Image resolution: w, h

    std::vector<glm::vec3> _pixels;  // Pixel array
};
