#pragma once

#include "types.h"
#include "FreeImage.h"


// freeimage class wrapper
class Image
{
public:
    Image(const String& fileName);
    ~Image();

    void save(const String& fileName);

protected:
    FIBITMAP* _bitmap = nullptr;

};
