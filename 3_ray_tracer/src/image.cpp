#include <iostream>
#include "image.h"


Image::Image(const String &fileName)
{
    // TODO: implement loading from file

    /*
     *
     * http://graphics.stanford.edu/courses/cs148-10-summer/docs/UsingFreeImage.pdf
     *
     *
     *
    FREE_IMAGE_FORMAT formato = FreeImage_GetFileType("image.png",0);//Automatocally detects the format(from over 20 formats!)
       FIBITMAP* imagen = FreeImage_Load(formato, "image.png");

       FIBITMAP* temp = imagen;
       imagen = FreeImage_ConvertTo32Bits(imagen);
       FreeImage_Unload(temp);

       int w = FreeImage_GetWidth(imagen);
       int h = FreeImage_GetHeight(imagen);
       //cout<<"The size of the image is: "<<textureFile<<" es "<<w<<"*"<<h<<endl; //Some debugging code

       GLubyte* textura = new GLubyte[4*w*h];
       char* pixeles = (char*)FreeImage_GetBits(imagen);
       //FreeImage loads in BGR format, so you need to swap some bytes(Or use GL_BGR).

       for(int j= 0; j<w*h; j++){
           textura[j*4+0]= pixeles[j*4+2];
           textura[j*4+1]= pixeles[j*4+1];
           textura[j*4+2]= pixeles[j*4+0];
           textura[j*4+3]= pixeles[j*4+3];
           //cout<<j<<": "<<textura[j*4+0]<<"**"<<textura[j*4+1]<<"**"<<textura[j*4+2]<<"**"<<textura[j*4+3]<<endl;
       }

       //Now generate the OpenGL texture object
       GLuint texturaID;
       glGenTextures(1, &amp;texturaID);
       glBindTexture(GL_TEXTURE_2D, texturaID);
       glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA, w, h, 0, GL_RGBA,GL_UNSIGNED_BYTE,(GLvoid*)textura );
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   */
}

Image::~Image()
{
    delete _bitmap;
}

void Image::save(const String &fileName)
{
    if (!_bitmap) {
        std::cout << "Can't save empty bitmap to " << fileName << std::endl;
        return;
    }

    // TODO: implement for all image types
    if (FreeImage_Save (FIF_PNG, _bitmap, fileName.c_str(), 0)) {
        std::cout << " Image " << fileName << "successfully saved ! " << std::endl;
    }
}
