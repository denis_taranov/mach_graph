#include "tracer.h"

using namespace glm;



SRay Tracer::createRay(glm::uvec2 pixelPos)
{
    return SRay();
}

glm::vec3 Tracer::TraceRay(SRay ray)
{
    vec3 color(0, 0, 1);
    return color;
}

Image Tracer::renderImage(int xRes, int yRes)
{
    /*
  // Reading input texture sample
  CImage* pImage = LoadImageFromFile("data/disk_32.png");
  if(pImage->GetBPP() == 32)
  {
    auto pData = (unsigned char*)pImage->GetBits();
    auto pCurrentLine = pData;
    int pitch = pImage->GetPitch();

    for(int i = 0; i < pImage->GetHeight(); i++) // Image lines
    {
      for(int j = 0; j < pImage->GetWidth(); j++) // Pixels in line
      {
        unsigned char b = pCurrentLine[j * 4];
        unsigned char g = pCurrentLine[j * 4 + 1];
        unsigned char r = pCurrentLine[j * 4 + 2];
        unsigned char alpha = pCurrentLine[j * 4 + 3];
      }
    }
  }

  // Rendering
  m_camera.m_resolution = uvec2(xRes, yRes);
  m_camera.m_pixels.resize(xRes * yRes);

  for(int i = 0; i < yRes; i++)
    for(int j = 0; j < xRes; j++)
    {
      SRay ray = MakeRay(uvec2(j, i));
      m_camera.m_pixels[i * xRes + j] = TraceRay(ray);
    }
    */
   return Image("");
}

Scene* Tracer::getScene()
{
    return _scene;
}

void Tracer::setScene(Scene *scene)
{
    _scene = scene;
}
