#include <iostream>
#include <fstream>

#include "tracer.h"


int main(int argc, char** argv)
{
    // Default resolution
    int xRes = 1024;
    int yRes = 768;

    if (argc != 2) {
        std::cout << "usage: tracer config" << std::endl;
        return 1;
    }

    std::ifstream in(argv[1]);
    if (!in.is_open()) {
        std::cout << "Invalid config path: " << argv[1] << std::endl;
        return 2;
    }

    in >> xRes >> yRes;
    in.close();


    Tracer tracer;
    Scene scene;

    tracer.setScene(&scene);
    Image image = tracer.renderImage(xRes, yRes);
    image.save("Result.png");

    return 0;
}
