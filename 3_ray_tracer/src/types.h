#pragma once

#include <vector>
#include <string>

#include "glm/glm.hpp"


using String = std::string;


struct SRay
{
  glm::vec3 _start;
  glm::vec3 _dir;
};


struct SMesh
{
  std::vector<glm::vec3> _vertices;  // vertex positions
  std::vector<glm::uvec3> _triangles;  // vetrex indices
};
