#include <string>
#include <vector>
#include <fstream>
#include <cassert>
#include <iostream>
#include <cmath>

#include "classifier.h"
#include "EasyBMP.h"
#include "linear.h"
#include "argvparser.h"
#include "image.h"


using std::string;
using std::vector;
using std::ifstream;
using std::ofstream;
using std::pair;
using std::make_pair;
using std::cout;
using std::cerr;
using std::endl;


using CommandLineProcessing::ArgvParser;

typedef vector<pair<BMP*, int> > TDataSet;
typedef vector<pair<string, int> > TFileList;
typedef vector<pair<vector<float>, int> > TFeatures;


// Load list of files and its labels from 'data_file' and
// stores it in 'file_list'
void LoadFileList(const string& data_file, TFileList* file_list) {
    ifstream stream(data_file.c_str());

    string filename;
    int label;
    
    int char_idx = data_file.size() - 1;
    for (; char_idx >= 0; --char_idx)
        if (data_file[char_idx] == '/' || data_file[char_idx] == '\\')
            break;
    string data_path = data_file.substr(0,char_idx+1);
    
    while(!stream.eof() && !stream.fail()) {
        stream >> filename >> label;
        if (filename.size())
            file_list->push_back(make_pair(data_path + filename, label));
    }

    stream.close();
}

// Load images by list of files 'file_list' and store them in 'data_set'
void LoadImages(const TFileList& file_list, TDataSet* data_set) {
    for (size_t img_idx = 0; img_idx < file_list.size(); ++img_idx) {
        // Create image
        BMP* image = new BMP();
        // Read image from file
        image->ReadFromFile(file_list[img_idx].first.c_str());
        // Add image and it's label to dataset
        data_set->push_back(make_pair(image, file_list[img_idx].second));
    }
}

// Save result of prediction to file
void SavePredictions(const TFileList& file_list,
                     const TLabels& labels,
                     const string& prediction_file) {
    // Check that list of files and list of labels has equal size
    assert(file_list.size() == labels.size());
    // Open 'prediction_file' for writing
    ofstream stream(prediction_file.c_str());

    // Write file names and labels to stream
    for (size_t image_idx = 0; image_idx < file_list.size(); ++image_idx)
        stream << file_list[image_idx].first << " " << labels[image_idx] << endl;
    stream.close();
}

// Get descriptors from Matrix <int> and put it into features vector
void DescriptorsPyramid(vector <float>& features, const Image& image)
{
    // h, w - 2, because SobelOp decrease size of image
    const uint w = image.n_cols - 2;
    const uint h = image.n_rows - 2;

    GrayImage gray = GrayScale(image);

    GrayImage Gx = gray.unary_map(SobelX());
    GrayImage Gy = gray.unary_map(SobelY());

    const uint resize_index = 8, quant_size = 36;
    const uint x_size = h / resize_index, y_size = w / resize_index;
    const uint nh = resize_index * x_size, nw = resize_index * y_size;

    vector <float> hog(quant_size);

    float gradient_rad, gradient_module, norm;
    uint gradient_degrees, tmp, gx, gy;

    for (uint x = 0; x < nh; x += x_size) {
        for (uint y = 0; y < nw; y += y_size) {
            std::fill(hog.begin(), hog.end(), 0);
            norm = 0;
            for (uint i = x; i < x + x_size && i < nh; ++i)
                for (uint j = y; j < y + y_size && j < nw; ++j) {

                    gx = Gx(i,j), gy = Gy(i,j);

                    gradient_rad = atan2(gy, gx);
                    gradient_degrees = gradient_rad * 180.0 / M_PI;

                    tmp = gy*gy + gx*gx;
                    norm += tmp;
                    gradient_module = sqrt(tmp);

                    hog[(gradient_degrees + 180) / (360 / quant_size)] += gradient_module;
                }

            norm = sqrt(norm);
            for (float value : hog) {
                features.push_back((norm > 0) ? (value / norm) : value );
            }
        }
    }
}

void ColorFeatures(vector <float>& features, const Image& image)
{
    const uint w = image.n_cols;
    const uint h = image.n_rows;

    const uint resize_index = 8;
    const uint x_size = h / resize_index, y_size = w / resize_index;
    const uint nh = resize_index * x_size, nw = resize_index * y_size;

    uint mean_R, mean_G, mean_B, r, g, b, cnt, tmp;

    for (uint x = 0; x < nh; x += x_size) {
        for (uint y = 0; y < nw; y += y_size) {

            cnt = 0, mean_R = 0, mean_G = 0, mean_B = 0;

            for (uint i = x; i < x + x_size && i < nh; ++i)
                for (uint j = y; j < y + y_size && j < nw; ++j) {
                    tie(r, g, b) = image(i, j);
                    mean_R += r, mean_G += g, mean_B += b;
                    cnt++;
                }

            tmp = cnt*255;
            mean_R /= tmp, mean_G /= tmp, mean_B /= tmp;

            features.push_back(mean_R);
            features.push_back(mean_G);
            features.push_back(mean_B);
        }
    }
}

void KernelForSVM(const vector <float>& image_features, vector <float>& non_linear_kernel)
{
    const float n = 2, L = 0.5;
    float sn, cs, k, tmp;
    for (float feature : image_features) {
        for (float lambda = -n*L; lambda <= n*L; lambda += L) {

            tmp = M_PI*lambda;
            k = 2 / (exp(tmp) + exp(-tmp));
            k = sqrt(feature*k);

            tmp = lambda*log(feature);
            cs = (feature > 0) ?  cos(tmp) : 0;
            sn = (feature > 0) ? -sin(tmp) : 0;

            non_linear_kernel.push_back(cs*k);
            non_linear_kernel.push_back(sn*k);
        }
    }
}

// Extract features from dataset.
void ExtractFeatures(const TDataSet& data_set, TFeatures* features) {
    for (auto& data : data_set) {

        BMP* bmp = data.first;
        Image image = BMPToImage(*bmp);

        const uint w = image.n_cols;
        const uint h = image.n_rows;

        vector <float> image_features;

        // Descriptors into one vector
        DescriptorsPyramid(image_features, image);
        DescriptorsPyramid(image_features, image.submatrix(0, 0, h/2, w/2));
        DescriptorsPyramid(image_features, image.submatrix(0, w/2, h/2, w/2));
        DescriptorsPyramid(image_features, image.submatrix(h/2, 0, h/2, w/2));
        DescriptorsPyramid(image_features, image.submatrix(h/2, w/2, h/2, w/2));

        // Color features of image
        ColorFeatures(image_features, image);

        // Nonlinear kernel SVM
        vector <float> non_linear_kernel;
        KernelForSVM(image_features, non_linear_kernel);
        features->push_back(make_pair(non_linear_kernel, data.second));
    }
}

// Clear dataset structure
void ClearDataset(TDataSet* data_set) {
    // Delete all images from dataset
    for (size_t image_idx = 0; image_idx < data_set->size(); ++image_idx)
        delete (*data_set)[image_idx].first;
    // Clear dataset
    data_set->clear();
}

// Train SVM classifier using data from 'data_file' and save trained model
// to 'model_file'
void TrainClassifier(const string& data_file, const string& model_file) {
    // List of image file names and its labels
    TFileList file_list;
    // Structure of images and its labels
    TDataSet data_set;
    // Structure of features of images and its labels
    TFeatures features;
    // Model which would be trained
    TModel model;
    // Parameters of classifier
    TClassifierParams params;
    
    // Load list of image file names and its labels
    LoadFileList(data_file, &file_list);
    // Load images
    LoadImages(file_list, &data_set);
    // Extract features from images
    ExtractFeatures(data_set, &features);

    // PLACE YOUR CODE HERE
    // You can change parameters of classifier here
    params.C = 0.05;
    TClassifier classifier(params);
    // Train classifier
    classifier.Train(features, &model);
    // Save model to file
    model.Save(model_file);
    // Clear dataset structure
    ClearDataset(&data_set);
}

// Predict data from 'data_file' using model from 'model_file' and
// save predictions to 'prediction_file'
void PredictData(const string& data_file,
                 const string& model_file,
                 const string& prediction_file) {
    // List of image file names and its labels
    TFileList file_list;
    // Structure of images and its labels
    TDataSet data_set;
    // Structure of features of images and its labels
    TFeatures features;
    // List of image labels
    TLabels labels;

    // Load list of image file names and its labels
    LoadFileList(data_file, &file_list);
    // Load images
    LoadImages(file_list, &data_set);
    // Extract features from images
    ExtractFeatures(data_set, &features);

    // Classifier
    TClassifier classifier = TClassifier(TClassifierParams());
    // Trained model
    TModel model;
    // Load model from file
    model.Load(model_file);
    // Predict images by its features using 'model' and store predictions
    // to 'labels'
    classifier.Predict(features, model, &labels);

    // Save predictions
    SavePredictions(file_list, labels, prediction_file);
    // Clear dataset structure
    ClearDataset(&data_set);
}

int ParseArgs(ArgvParser& parser, int argc, char** argv) {
    // Description of program
    parser.setIntroductoryDescription("Machine graphics course, task 2. CMC MSU, 2014.");
    // Add help option
    parser.setHelpOption("h", "help", "Print this help message");
    // Add other options
    parser.defineOption("data_set", "File with dataset",
                        ArgvParser::OptionRequiresValue | ArgvParser::OptionRequired);
    parser.defineOption("model", "Path to file to save or load model",
                        ArgvParser::OptionRequiresValue | ArgvParser::OptionRequired);
    parser.defineOption("predicted_labels", "Path to file to save prediction results",
                        ArgvParser::OptionRequiresValue);
    parser.defineOption("train", "Train classifier");
    parser.defineOption("predict", "Predict dataset");

    // Add options aliases
    parser.defineOptionAlternative("data_set", "d");
    parser.defineOptionAlternative("model", "m");
    parser.defineOptionAlternative("predicted_labels", "l");
    parser.defineOptionAlternative("train", "t");
    parser.defineOptionAlternative("predict", "p");

    // Parse options
    return parser.parse(argc, argv);
}


int main(int argc, char** argv) {

    ArgvParser parser;
    int parse_result = ParseArgs(parser, argc, argv);
    if (parse_result) {
        cout << parser.parseErrorDescription(parse_result) << endl;
        return parse_result;
    }

    // Get values
    string data_file = parser.optionValue("data_set");
    string model_file = parser.optionValue("model");
    bool train = parser.foundOption("train");
    bool predict = parser.foundOption("predict");

    // If we need to train classifier
    if (train) {
        TrainClassifier(data_file, model_file);
    }
    // If we need to predict data
    if (predict) {
        // You must declare file to save images
        if (!parser.foundOption("predicted_labels")) {
            cerr << "Error! Option --predicted_labels not found!" << endl;
            return 1;
        }
        // File to save predictions
        string prediction_file = parser.optionValue("predicted_labels");
        // Predict data
        PredictData(data_file, model_file, prediction_file);
    }
}
