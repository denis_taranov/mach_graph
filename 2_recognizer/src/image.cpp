#include "image.h"
#include "EasyBMP.h"

#include <iostream>
using namespace std;


Image BMPToImage(BMP& bmp) {
    const int w = bmp.TellWidth();
    const int h = bmp.TellHeight();

    Image image(h, w);
    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < w; ++j) {
            RGBApixel *p = bmp(j, i);
            image(i, j) = make_tuple(p->Red, p->Green, p->Blue);
        }
    }

    return image;
}

GrayImage GrayScale(const Image& image) {
    const int w = image.n_cols;
    const int h = image.n_rows;
    uint r, g, b;

    GrayImage gray(h, w);
    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < w; ++j) {
            tie(r, g, b) = image(i, j);
            gray(i, j) = 0.299*r + 0.587*g + 0.114*b;
        }
    }

    return gray;
}

void GrayImageToBMP(const GrayImage& image, BMP& bmp) {
    const int w = image.n_cols;
    const int h = image.n_rows;

    bmp.SetSize(w, h);

    RGBApixel p;
    p.Alpha = 255;

    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < w; ++j) {
            p.Red = p.Green = p.Blue = image(i, j);
            bmp.SetPixel(j, i, p);
        }
    }
}

void ImageToBMP(const Image& image, BMP& bmp) {
    const int w = image.n_cols;
    const int h = image.n_rows;

    bmp.SetSize(w, h);

    RGBApixel p;
    p.Alpha = 255;

    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < w; ++j) {
            tie(p.Red, p.Green, p.Blue) = image(i, j);
            bmp.SetPixel(j, i, p);
        }
    }
}


// Sobel filters

int SobelX::operator() (const GrayImage& image) const {
    return  + 1 * image(0,0) - 1 * image(0,2)
            + 2 * image(1,0) - 2 * image(1,2)
            + 1 * image(2,0) - 1 * image(2,2);
}

int SobelY::operator() (const GrayImage& image) const {
    return  + 1 * image(0,0) + 2 * image(0,1) + 1 * image(0,2)
            - 1 * image(2,0) - 2 * image(2,1) - 1 * image(2,2);
}
