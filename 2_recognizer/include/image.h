#ifndef IMAGE_H
#define IMAGE_H

#include "matrix.h"

using std::tuple;
using std::make_tuple;
using std::tie;

using Image = Matrix<tuple<int, int, int>>;
using GrayImage = Matrix<int>;
class BMP;


Image BMPToImage(BMP& image);
GrayImage GrayScale(const Image& image);

void GrayImageToBMP(const GrayImage& image, BMP& bmp);
void ImageToBMP(const Image& image, BMP& bmp);


class SobelX {
public:
    static const int vert_radius = 1;
    static const int hor_radius = 1;

    int operator() (const GrayImage& image) const;
};

class SobelY {
public:
    static const int vert_radius = 1;
    static const int hor_radius = 1;

    int operator() (const GrayImage& image) const;
};


#endif // IMAGE_H
